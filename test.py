import _3DXY_script as xy

parms = xy.default_parm_dict()

Lxy = 512
Lz = 4
frus = 1/512

term = 10000

parms['Lz'] = Lz
parms['Lxy'] = Lxy
parms['frustration'] = frus
parms['term_steps'] = term
parms['SM'] = .1
   
print(parms)

d, t = xy.run_XY(xy.random_phase(Lxy**2*Lz), 10000, parms)    

print("elapsed time = %f"%t)

