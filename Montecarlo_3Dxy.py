#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import numpy as np
import sys
from collections import OrderedDict

parse = argparse.ArgumentParser('3D XY Montecarlo for Layered Type II Superconductors')

parse.add_argument('-Tm_steps', type=np.float32, help='termalization steps', default=np.int32(100000))

parse.add_argument('-SM', type=np.float32, help='Magnetic penetration', default=np.float32(.0001))

parse.add_argument('-T', type=np.float32, help='Temperatura', default=np.float32(.02))

parse.add_argument('-Lxy', type=np.int32, help='Tamaño del lado en el plano XY', default=np.int32(72))

parse.add_argument('-Lz', type=np.int32, help='Número de capas en el eje Z', default=np.int32(4))

parse.add_argument('-f', type=np.float32, help='Valor  de la frustración según f = m/Lxy, donde m enteros', default=np.float32(1./36.))

parse.add_argument('-G', type=np.float32, help='Anisotropía en el eje Z', default=np.float32(25000))

parse.add_argument('-ins', type=np.int32, help='Paso de inicio, importante para la generación de los números aleatoreos', default=np.int32(0))

parse.add_argument('-ms', type=np.int32, help='Pasos montecarlo', default=np.int32(50000))

parse.add_argument('-seed', type=np.int32, help='semilla', default=np.int32(0))

args = parse.parse_args()

parms_dict = OrderedDict()    

parms_dict['term_steps'] = args.Tm_steps
parms_dict['SM'] = args.SM
parms_dict['temperatura'] = args.T
parms_dict['Lxy'] = args.Lxy
parms_dict['Lz'] = args.Lz
parms_dict['frustration'] = np.float32(args.f)
parms_dict['gamma'] = args.G
parms_dict['seed'] = args.seed

m_steps = args.ms

from _3DXY_script import *

if __name__ == '__main__':
    
    #for k, v in parms_dict.items():
    #    print('{}\t{}'.format(k,type(v)))

    N = parms_dict['Lxy']**2*parms_dict['Lz']
    
    init_conf = random_phase(N)

    tt = np.linspace(.0001, .05, 6, endpoint=True)[::-1]

    temps = tt#np.concatenate((tt, tt[:-1][::-1]))

    print(temps)

    parms_dict['temperatura'] = temps[0]

    parms_dict['term_steps'] = parms_dict['term_steps'] + 200000

    current_state_dict, delta_t = run_XY(init_conf, m_steps, parms_dict)
    
    h5f = h5file('cyclefrus=%f_G=%f_Lz=%d_Lxy=%d'%(args.f, args.G, args.Lz, args.Lxy))
    
    h5f.write('helf-half', m_steps, delta_t, parms_dict, **current_state_dict)

    parms_dict['term_steps'] = args.Tm_steps

    for T in temps[1:]:
        
        parms_dict['temperatura'] = T
        
        current_state_dict, delta_t = run_XY(current_state_dict['phases'].flatten(), m_steps, parms_dict,
                                             paso=current_state_dict['paso'])
        
        h5f.write('helf-half', m_steps, delta_t, parms_dict, new_group=True, **current_state_dict)
    
    print('\n\nDone :)')
