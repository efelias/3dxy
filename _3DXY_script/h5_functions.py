#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Module docstring"""

import datetime as date
import h5py as h5
import pycuda.driver as cuda
import numpy as np

__all__ = ['h5file']

class h5file:
    """Clase para el manejo de archivos hdf5 para el guardado de resutados
    de simulaciones.
    """
    def __init__(self, file_name, group_name=None):
        self.file_name = file_name
        if group_name is None:
            self.group_name = date.datetime.now().isoformat()[:-7]
        else:
            self.group_name = group_name

    def write(self, initial_conf, MC_steps, delta_t, parm_dict, new_group=False, **state_dict):
        """
        Crea un archivo .h5, si es q no existe, o lo abre, si es q existe.
        Luego guarda una simulacion en un directorio según fecha y hora.

        Los parámetros de la simulación (condicion_inicial, pasos_montecarlo, delta_t, parm_dict)
        se almacenan como atributos del directório donde parm_dict es un dicionario
        q con parámetros específicos.

        Los resultados de la simulacíon se especifican con el diccionario state_dict
        y se almacenan como data_set dentro del directorio.
        """
        file_temp = h5.File(self.file_name+'.h5', 'a')
        
        if new_group:
        	self.group_name = date.datetime.now().isoformat()[:-7]

        group_temp = file_temp.create_group(self.group_name)

        for k, v in parm_dict.items():
            group_temp.attrs[k] = v

        placa = cuda.Context.get_device()
        group_temp.attrs['video_card'] = placa.name()
        group_temp.attrs['MC_steps'] = MC_steps
        group_temp.attrs['running_time'] = delta_t
        group_temp.attrs['initial_conf'] = initial_conf

        for k, v in state_dict.items():
            if np.ndim(v) == 1:
                group_temp.create_dataset(k, data=v, maxshape=(None, ))
            else:
                group_temp.create_dataset(k, data=v)
        file_temp.close()

    def checkpoint(self, MC_steps, delta_t, **state_dict):

        file_temp = h5.File(self.file_name+'.h5', 'a')
        group_temp = file_temp[self.group_name]

        group_temp.attrs['running_time'] = group_temp.attrs['running_time'] + delta_t
        group_temp.attrs['MC_steps'] = MC_steps + group_temp.attrs['MC_steps']
        for k, v in state_dict.items():

            if np.ndim(v) == 1:
                data_set_temp = group_temp[k]
                data_temp = data_set_temp.value

                new_shape = len(data_temp)+len(v)

                data_set_temp.resize((new_shape, ))
                data_set_temp[...] = np.append(data_temp, v)

            else:
                data_set_temp = group_temp[k]
                data_set_temp[...] = v
        file_temp.close()

