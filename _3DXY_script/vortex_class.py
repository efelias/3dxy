#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function
import sys
#############################################
from .gpu_functions import *
import pycuda.autoinit
import pycuda.gpuarray as gy
import pycuda.cumath as cm
#############################################
import numpy as np
import os
from .extra_functions import *

__all__ = ['vortex', 'set_state_dict']

def set_state_dict(**state_dict):

    keys = ('phases', 'aceptancia', 'sinz', 'paso', 'carga', 'energia', 'cosz', 'campo','mean_carge')
    default_values = (None, [], [], 0, None, [], [], [], None)
    current_state_dict = {}

    for i, k in enumerate(keys):
        if k in state_dict.keys():
            current_state_dict[k] = state_dict[k]
        else:
            current_state_dict[k] = default_values[i]
    return current_state_dict


def maxe_triangular_A(L1, L2, f):
    q0 = lambda b : 2*np.pi*np.sqrt(2*b/np.sqrt(3))
    q1 = np.array([1, .5, -.5])
    q2 = np.array([0, np.sqrt(3)/2, np.sqrt(3)/2])
    j1 = np.array([-1-1j, 1-1j, -1-1j])
    j2 = np.array([-1-1j, 1-1j, 1+1j])
    
    ranx1 = np.arange(L1, dtype=int)
    ranx2 = np.arange(L2, dtype=int)
    
    Ax1 = np.empty((L1, L2), dtype=np.float32)
    Ax2 = np.empty((L1, L2), dtype=np.float32)
    for x1 in ranx1:
        for x2 in ranx2:
            Ax1[x1, x2] = 2*np.sum((j1*np.exp(1j*q0(f)*(q1*(x1)+q2*(x2-.5)))).real)
            Ax2[x1, x2] = 2*np.sum((j2*np.exp(1j*q0(f)*(q1*(x1-.5)+q2*(x2)))).real)
    maxs = Ax1.max()
    mays = Ax2.max()
    return f*Ax1/maxs, f*Ax2/mays


class vortex:
    def __init__(self, intial_cond, parms_dict, **state_dict):
        ####################################################################
        #parametros
        ####################################################################
        self.parms = parms_dict
        self.N = self.parms['Lxy']**2*self.parms['Lz']
        self.N2d = self.parms['Lxy']**2
        ####################################################################
        #vectores GPU
        ####################################################################
        ##fases
        self.M_gpu = gy.to_gpu(intial_cond)
        ##vorticidad
        self.cargas_gpu = gy.empty(shape=(self.N,), dtype=np.int32)
        self.cum_carga = gy.zeros_like(self.cargas_gpu)
        ##energia por sitio
        self.energy_gpu = gy.empty(shape=(self.N,), dtype=np.float32)
        ##aceptancia por paso de montecarlo
        self.acep = gy.zeros(shape=(self.N,), dtype=np.int32)
        ##diferencias de fases entre planos
        self.dOz_gpu = gy.empty_like(self.energy_gpu)
        #corrientes
        self.jx = gy.empty((self.N,), dtype=np.float32)
        self.jy = gy.empty_like(self.jx)
        #tranformadas
        self.aq_alpha_re = gy.empty_like(self.jx)
        self.aq_alpha_im = gy.empty_like(self.jx)
        #Potenciales vector del sustrato
        self.Ax = gy.zeros((self.N2d,), dtype=np.float32)
        self.Ay = gy.zeros((self.N2d,), dtype=np.float32)
        self.b_sus = gy.empty_like(self.Ax)
        self.current_state_dict = set_state_dict(**state_dict)
        self.paso = self.current_state_dict['paso']

    def one_step(self):
    	for shift in [0, 1]:
    		metropolis(self.M_gpu, self.acep, self.Ax, self.Ay, shift, self.paso, *list(self.parms.values())[2:])
    	self.paso += 1

    def make_order(self, on):
        if on:
            ax, ay = maxe_triangular_A(self.parms['Lxy'], self.parms['Lxy'], self.parms['frustration'])
            self.Ax = gy.to_gpu(ax.flatten())
            self.Ay = gy.to_gpu(ay.flatten())
        if not on:
            self.Ay.fill(0.0)
            self.Ax.fill(0.0)

    def magnetic_coupling(self):
    	charge(self.M_gpu, self.cargas_gpu, self.Ax, self.Ay,  self.parms['Lxy'], self.parms['Lz'], self.parms['frustration'])
    	current(self.M_gpu, self.jx, self.jy, self.Ax, self.Ay, self.parms['Lxy'], self.parms['Lz'], self.parms['frustration'])
    	ft, qx, qy = strucuture_factor((self.cargas_gpu.get()).reshape((self.parms['Lz'], self.parms['Lxy'], self.parms['Lxy'])), self.parms['Lxy'], self.parms['Lz'], self.parms['frustration'], n=6)
    	self.ft = ft
    	aq_x, aq_y = FTJ(self.jx, self.jy, self.aq_alpha_re, self.aq_alpha_im, qx, qy, self.parms['Lxy'], self.parms['Lz'], self.parms['SM'])
    	IFTA(self.Ax, self.Ay, aq_x, aq_y, qx, qy, self.parms['Lxy'])

    def get_ft(self, compute=False):
    	if compute:
    		ft, qx, qy = strucuture_factor((self.cargas_gpu.get()).reshape((self.parms['Lz'], self.parms['Lxy'], self.parms['Lxy'])), self.parms['Lxy'], self.parms['Lz'], self.parms['frustration'], n=6)
    		self.ft = ft
    	return self.ft

    def messure_globals(self):
        self.current_state_dict['aceptancia'].append(float(gy.sum(self.acep).get())/self.N)

        total_e, cos_z, sin_z = energy(self.M_gpu, self.energy_gpu, self.dOz_gpu, self.Ax, self.Ay, self.parms['Lxy'], self.parms['Lz'], self.parms['frustration'],  self.parms['gamma']) 

        self.current_state_dict['energia'].append(total_e)
        self.current_state_dict['cosz'].append(cos_z)
        self.current_state_dict['sinz'].append(sin_z)
    
    def acumulate_carge(self, compute=False):
    	if compute:
    		charge(self.M_gpu, self.cargas_gpu, self.Ax, self.Ay, self.parms['Lxy'], self.parms['Lz'], self.parms['frustration'])
    		self.cum_carga += self.cargas_gpu
    	else:
    		self.cum_carga += self.cargas_gpu

    def messure_locals(self,time_normalization):
        self.current_state_dict['carga'] = self.cargas_gpu.get().reshape((self.parms['Lz'],
                                                                          self.parms['Lxy'],
                                                                          self.parms['Lxy']))
        self.current_state_dict['mean_carge'] = (np.float32(self.cum_carga.get())/time_normalization).reshape((self.parms['Lz'],
                                                                                                               self.parms['Lxy'],
                                                                                                               self.parms['Lxy']))
        self.current_state_dict['phases'] = self.M_gpu.get().reshape((self.parms['Lz'],
                                                                      self.parms['Lxy'],
                                                                      self.parms['Lxy']))
        self.current_state_dict['paso'] = self.paso
        campo(self.Ax, self.Ay, self.b_sus, self.parms['Lxy'])
        self.current_state_dict['campo'] = self.b_sus.get().reshape((self.parms['Lxy'],
                                                                     self.parms['Lxy']))

    def get_charge(self, compute=False):
    	if compute:
    		charge(self.M_gpu, self.cargas_gpu, self.Ax, self.Ay, self.parms['Lxy'], self.parms['Lz'], self.parms['frustration'])
    	return self.cargas_gpu

    def get_field(self):
    	campo(self.Ax, self.Ay, self.b_sus, self.parms['Lxy'])
    	return self.b_sus

    def get_state(self):
    	return self.current_state_dict

    def change_parm(self, key, value):
    	self.parms[key] = value

    def free_data(self):
    	for i in [self.M_gpu, self.cargas_gpu, self.energy_gpu, self.acep, self.dOz_gpu, self.jx, self.jy, self.aq_alpha_re, self.aq_alpha_im, self.Ax, self.Ay, self.b_sus]:
    		i.gpudata.free()

