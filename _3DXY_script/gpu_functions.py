#!/usr/bin/env python
# coding: utf-8
#############################################
import pycuda.autoinit
from pycuda.compiler import SourceModule
import pycuda.gpuarray as gy
import pycuda.cumath as cm
#############################################
import numpy as np
import os

__all__ = ['metropolis', 'charge', 'energy', 'current', 'FTJ', 'IFTA','campo']

mod = SourceModule(open("./_3DXY_script/funciones.h", 'r').read(), no_extern_c=True,
		include_dirs=[os.getcwd()+'/_3DXY_script'])
mod2 = SourceModule(open("./_3DXY_script/ft_kernel.h", 'r').read(), no_extern_c=True,
		include_dirs=[os.getcwd()+'/_3DXY_script'])

metropolis_kernel = mod.get_function("metropolis")
charge_kernel = mod.get_function("charge")
energy_kernel = mod.get_function("energy")
current_kernel = mod.get_function("current")

FTJ_kernel =  mod2.get_function("J_ft")
IFTA_kernel = mod2.get_function("Inv_ft_a")
B_sus_kernel = mod.get_function("B_sus")


def metropolis(phases_gpu, aceptancia_gpu, Ax_gpu, Ay_gpu, shift, paso, temperatura, Lxy, Lz, frustration, gamma, seed):

    shift = np.int32(shift); paso = np.int32(paso); temperatura = np.float32(temperatura)
    Lxy = np.int32(Lxy); Lz = np.int32(Lz); frustration = np.float32(frustration)
    gamma = np.float32(gamma); factor = np.float32(.53*np.sqrt(temperatura)); seed = np.int32(seed)
    
    N = Lxy**2*Lz
    th = 128
    bl = int((N+th-1)//th)

    metropolis_kernel(phases_gpu, aceptancia_gpu, Ax_gpu, Ay_gpu, shift, paso,
                      temperatura, Lxy, Lz, frustration, gamma, factor, seed,
                      block=(th, 1, 1), grid=(bl, 1, 1))

def charge(phases_gpu, vorticity_gpu,  Ax_gpu, Ay_gpu, Lxy, Lz, frustration):

    Lxy = np.int32(Lxy); Lz = np.int32(Lz); frustration = np.float32(frustration)

    N = Lxy**2*Lz
    th = 128
    bl = int((N+th-1)//th)

    charge_kernel(phases_gpu, vorticity_gpu, Ax_gpu, Ay_gpu, Lxy, Lz, frustration,
                  block=(th, 1, 1), grid=(bl, 1, 1))

def energy(phases_gpu, energy_gpu, dOz_gpu, Ax_gpu, Ay_gpu, Lxy, Lz, frustration, gamma):

    Lxy = np.int32(Lxy); Lz = np.int32(Lz); frustration = np.float32(frustration)
    gamma = np.float32(gamma)
    
    N = Lxy**2*Lz
    
    th = 128
    bl = int((N+th-1)//th)

    energy_kernel(phases_gpu, energy_gpu, dOz_gpu, Ax_gpu, Ay_gpu, Lxy, Lz, frustration,
                  gamma, block=(th, 1, 1), grid=(bl, 1, 1))

    total_energy = gy.sum(energy_gpu).get()/N
    cos_z = gy.sum(cm.cos(dOz_gpu)).get()/N
    sin_z = gy.sum(cm.sin(dOz_gpu)).get()/N
    
    return total_energy, cos_z, sin_z 

def current(phases_gpu, jx_gpu, jy_gpu, Ax_gpu, Ay_gpu, Lxy, Lz, frustration):

    Lxy = np.int32(Lxy); Lz = np.int32(Lz); frustration = np.float32(frustration)
    
    N = Lxy**2*Lz

    th = 128
    bl = int((N+th-1)//th)

    current_kernel(phases_gpu, jx_gpu, jy_gpu, Ax_gpu, Ay_gpu, Lxy, Lz, frustration,
                   block=(th, 1, 1), grid=(bl, 1, 1))

def FTJ(jx_gpu, jy_gpu, aq_alpha_re, aq_alpha_im, qx_list, qy_list, Lxy, Lz, SM):

    qx_list = np.array(qx_list, dtype=np.int32)
    qy_list = np.array(qy_list, dtype=np.int32)

    Lxy = np.int32(Lxy); Lz = np.int32(Lz); SM = np.float32(SM)

    N = Lxy**2*Lz
    th = 128
    bl = int((N+th-1)//th)
    
    a_x = []
    a_y = []

    for i in range(len(qx_list)):
        FTJ_kernel(jx_gpu, aq_alpha_re, aq_alpha_im, qx_list[i], qy_list[i], SM,
                   Lxy, Lz, np.int32(0), block=(th, 1, 1), grid=(bl, 1, 1))

        a_x.append([gy.sum(aq_alpha_re).get()/N, gy.sum(aq_alpha_im).get()/N])

        FTJ_kernel(jy_gpu, aq_alpha_re, aq_alpha_im, qx_list[i], qy_list[i], SM,
        	       Lxy, Lz, np.int32(1), block=(th, 1, 1), grid=(bl, 1, 1))

        a_y.append([gy.sum(aq_alpha_re).get()/N, gy.sum(aq_alpha_im).get()/N])

    a_x = np.array(a_x, dtype=np.float32)
    a_y = np.array(a_y, dtype=np.float32)

    return a_x, a_y

def IFTA(Ax_gpu, Ay_gpu, aq_x, aq_y, qx_list, qy_list, Lxy):

    Lxy = np.int32(Lxy)

    N = Lxy**2

    th = 128
    bl = int((N+th-1)//th)

    qx_list = np.array(qx_list, dtype=np.int32)
    qy_list = np.array(qy_list, dtype=np.int32)

    qx_d = gy.to_gpu(qx_list)
    qy_d = gy.to_gpu(qy_list)

    aq_x = np.array(aq_x, dtype=np.float32)
    aq_y = np.array(aq_y, dtype=np.float32)

    ax_re = gy.to_gpu(aq_x[:,0])
    ax_im = gy.to_gpu(aq_x[:,1])
    ay_re = gy.to_gpu(aq_y[:,0])
    ay_im = gy.to_gpu(aq_y[:,1])

    IFTA_kernel(Ax_gpu, ax_re, ax_im, qx_d, qy_d, np.int32(len(qx_list)), Lxy, np.int32(0),
    	        block=(th, 1, 1), grid=(bl, 1, 1))
    IFTA_kernel(Ay_gpu, ay_re, ay_im, qx_d, qy_d, np.int32(len(qx_list)), Lxy, np.int32(1),
    	        block=(th, 1, 1), grid=(bl, 1, 1))

def campo(Ax_gpu, Ay_gpu, b_gpu, Lxy):
    Lxy = np.int32(Lxy)

    N = Lxy**2

    th = 128
    bl = int((N+th-1)//th)
    
    B_sus_kernel(Ax_gpu, Ay_gpu, b_gpu, Lxy, block=(th, 1, 1), grid=(bl, 1, 1))
