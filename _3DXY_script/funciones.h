#include <Random123/philox.h>
#include <Random123/u01.h>


//__device__ float uniform(int n, int seed, int t);
//extern "C" __device__ int delta_E(int *array,int i, int L );

#define PI 3.141592654f
typedef r123::Philox2x32 RNG; 


__device__ float2 uniform(int n, int seed, int t)
{   /*
    Generador de numeros aleatorios en la memoria individual de cada Tread.
    Recive tres enteros de argumento y retorna una tupla (tipo float2 de CUDA) de dos
    nueros aleatoreo "unicos" para una dada terna de enteros.
    El resultado puede ser regenerado en cualquier momento con la misma terna de enteros.
    */

    // keys and counters 
    RNG philox;     
    RNG::ctr_type c={{}};
    RNG::key_type k={{}};
    RNG::ctr_type r;
    // Garantiza una secuencia random "unica" para cada thread  
    k[0]=n; 
    c[1]=seed;
    c[0]=t;
    r = philox(c, k); 
    float2 num;
    num.x=u01_closed_closed_32_53(r[0]);
    num.y=u01_closed_closed_32_53(r[1]);
    return num;
}

extern "C" {
__device__ float mod_2pi(float dO)
{	/*
	Asegura  que los valores de los angulos esten entre -PI y PI
	*/
    if (dO < -PI) {dO+=2*PI;}
    if (dO > PI ) {dO-=2*PI;}
    return dO;
}
}

extern "C" {
__device__ float mod_2pi2(float x)
{   /*
    Asegura  que los valores de los angulos esten entre -PI y PI
    */
    float dO = x-2*PI*floor(x/(2*PI));
    if (dO < -PI) {dO+=2*PI;}
    if (dO > PI ) {dO-=2*PI;}
    return dO;
}
}


extern "C" {
__device__ float d_Exy(float angle_i,float flip_angle,float neig_angle,float frus,int sentido)
{   /*
    Diferencia de energía para un modelo tipo XY con interaccion del tipo -cos(delta_angle).
    Argumentos:
        angle_i: el angulo de un dado sitio,
        flip_angle: el cambio propuesto para el angulo en el sitio (algoritmo de metropolis)
        neig_angle:  al angulo de alǵun vecino con el q se quiere calcular la diferencia de energía,
        frus: es la frustracion entre el bond q une el sitio con el vecino,
        sentido: puede valer 1 o -1 y garantiza q las diferencias se realicen siempre en la misma direccion (nostese
        q esto solo es importante si dicho bond tiene frustracion, ya q cos() es una funcion par).
    */
    float O_i = mod_2pi2(sentido*(neig_angle-angle_i)-frus);
    float O_f = mod_2pi2(sentido*(neig_angle-flip_angle)-frus);

    float dEi = cosf(O_i)-cosf(O_f);
    return dEi;
}
}

extern "C" {
__device__ float Vx(float angle)
{   /*
	Potencial propuesto por Koshelev (PhysRevB.56.11201) para minimizar el pining del sistema a la red cuadrada
	*/
    float r = .37565;
    return -3*r/4-(1-r)*cosf(angle)-(r/4)*cosf(2*angle);
}
}


extern "C" {
__device__ float d_EV(float angle_i,float flip_angle,float neig_angle,float frus,int sentido)
{   /*
    Diferencia de energía para un modelo tipo XY con interaccion del tipo -3*r/4-(1-r)*cos(angle)-(r/4)*cos(2*angle).
    Argumentos:
        angle_i: el angulo de un dado sitio,
        flip_angle: el cambio propuesto para el angulo en el sitio (algoritmo de metropolis)
        neig_angle:  al angulo de alǵun vecino con el q se quiere calcular la diferencia de energía,
        frus: es la frustracion entre el bond q une el sitio con el vecino,
        sentido: puede valer 1 o -1 y garantiza q las diferencias se realicen siempre en la misma direccion (nostese
        q esto solo es importante si dicho bond tiene frustracion, ya q cos() es una funcion par).
    */
    float O_i = mod_2pi2(sentido*(neig_angle-angle_i)-frus);
    float O_f = mod_2pi2(sentido*(neig_angle-flip_angle)-frus);

    float dEi = Vx(O_f)-Vx(O_i);
    return dEi;
}
}

extern "C" {
__global__ void metropolis(float *M,int *acep, float *Ax, float *Ay, int shift, int t, float temperatura, int Lxy, int Lz,float frustration,
                           float gamma,float factor, int seed)
{
	int N=Lxy*Lxy*Lz;
	int id=blockIdx.x*blockDim.x+threadIdx.x;

	if (id<N/2){
    
        int z=int((2*id)/Lxy/Lxy);

        int n = (2*id) + (int((2*id)/Lxy)+(z%2+shift)%2)%2;

        int nx=n%Lxy;
        int ny=int(n/Lxy)%Lxy;
        int nz=int(n/Lxy/Lxy);

        int n2d = nx + ny*Lxy;

        int arriba = nx+((ny-1+Lxy)%Lxy)*Lxy+nz*Lxy*Lxy;
        int abajo = nx+((ny+1)%Lxy)*Lxy+nz*Lxy*Lxy;
        
        int derecha = (nx+1)%Lxy+ny*Lxy+nz*Lxy*Lxy;
        int izquierda = (nx-1+Lxy)%Lxy+ny*Lxy+nz*Lxy*Lxy;
        
        int atras = nx+ny*Lxy+((nz+1)%Lz)*Lxy*Lxy;
        int adelante = nx+ny*Lxy+((nz-1+Lz)%Lz)*Lxy*Lxy;

        float fy = 2*PI*frustration*nx+Ay[n2d];
        float fx = Ax[n2d];

        float fy2 = 2*PI*frustration*nx+Ay[nx+((ny-1+Lxy)%Lxy)*Lxy];
        float fx2 = Ax[(nx-1+Lxy)%Lxy+ny*Lxy];

        float2 rn = uniform(n, seed, t);

        float fliped_angle = mod_2pi2(M[n]+PI*(2*rn.x-1.)*factor);

		float dE = (
                d_EV(M[n],fliped_angle,M[abajo],fy,1)+
                d_EV(M[n],fliped_angle,M[arriba],fy2,-1)+
                d_EV(M[n],fliped_angle,M[derecha],fx,1)+
                d_EV(M[n],fliped_angle,M[izquierda],fx2,-1)+
                d_Exy(M[n],fliped_angle,M[atras],0.0,1)/gamma+
                d_Exy(M[n],fliped_angle,M[adelante],0.0,-1)/gamma
            );
		float p = exp(-dE/temperatura);
		if (rn.y < p) {
            M[n]=fliped_angle;
            acep[n] = 1;
        } else {acep[n] = 0;}
	}
}
}


extern "C" {
__device__ float d_theta(float angle1,float angle2,float frus)
{	
    float dO = mod_2pi2(angle2-angle1-frus);
    return dO;
}
}

extern "C" {
__global__ void charge(float *angle,int *q, float *Ax, float *Ay, int Lxy,int Lz,float frustration)
{   /*, 
    Calculo de la carga topologica por plaqueta para una configuracion dada de angulos en una red cuadrada.
    Los datos se leen del vector angle y en el verctor "q" se almacenan las cargas. 
    Los valores que se esperan obtener para "q" son 1, -1 o 0.
    Los valores de "L" y "frustracion" deben ser los mismos que los usados para la simulación de Montecarlos
    */
    int N=Lxy*Lxy*Lz;
    int n=blockIdx.x*blockDim.x+threadIdx.x;
    if (n<N){
        
        int nx=n%Lxy;
        int ny=int(n/Lxy)%Lxy;
        int nz=int(n/Lxy/Lxy);
        int n2d = nx + ny*Lxy;

        int arriba = nx+((ny+1+Lxy)%Lxy)*Lxy+nz*Lxy*Lxy;
        
        int derecha = (nx+1)%Lxy+ny*Lxy+nz*Lxy*Lxy;

        int diagonal = (nx+1)%Lxy + ((ny+1)%Lxy)*Lxy+nz*Lxy*Lxy;

        float fy = 2*PI*frustration*nx+Ay[n2d];

        float fx = Ax[n2d];

        float fy2 = 2*PI*frustration*((nx+1+Lxy)%Lxy)+Ay[nx+((ny+1+Lxy)%Lxy)*Lxy];
        float fx2 = Ax[(nx+1+Lxy)%Lxy+ny*Lxy];
        
        float b =( Ax[n2d]+Ay[(nx+1+Lxy)%Lxy+ny*Lxy]-Ax[nx+((ny+1+Lxy)%Lxy)*Lxy]-Ay[n2d])/2/PI;

        q[n]=int (nearbyintf(frustration+b+
        	                 (d_theta(angle[n],angle[derecha],fx)+
        		              d_theta(angle[derecha],angle[diagonal],fy2)-
        		              d_theta(angle[arriba],angle[diagonal],fx2)-
        		              d_theta(angle[n],angle[arriba],fy))/(2*PI)
        	                 )
                  );
    }
}
}

extern "C" {
__global__ void B_sus( float *Ax, float *Ay, float *b, int Lxy){

    int N = Lxy*Lxy;
    int n2d = blockIdx.x*blockDim.x+threadIdx.x;
    if (n2d < N){
        int nx = n2d%Lxy;
        int ny = int(n2d/Lxy);
        b[n2d] = (Ax[n2d]+Ay[(nx+1+Lxy)%Lxy+ny*Lxy]-Ax[nx+((ny+1+Lxy)%Lxy)*Lxy]-Ay[n2d])/2/PI;
    }
}    
}

extern "C" {
__global__ void energy(float *angle,float *Energy,float *dO_z, float *Ax, float *Ay, int Lxy, int Lz, float frustration, float gamma)
{   /*
    Calcula la energia por sitio a partir del array de phases, como para luego realizar una reduccion
	y obtener la energia total.    
    */
    int N=Lxy*Lxy*Lz;
    int n=blockIdx.x*blockDim.x+threadIdx.x;
    if (n<N){

        int nx = n%Lxy;
        int ny = int(n/Lxy)%Lxy;
        int nz = int(n/Lxy/Lxy);
        int n2d = nx + ny*Lxy;

        int abajo = nx+((ny+1)%Lxy)*Lxy+nz*Lxy*Lxy;
        
        int derecha = (nx+1)%Lxy+ny*Lxy+nz*Lxy*Lxy;
        
        int atras = nx+ny*Lxy+((nz+1)%Lz)*Lxy*Lxy;

        float fy = 2*PI*frustration*nx+Ay[n2d];
        float fx = Ax[n2d];

        float O_y = mod_2pi2(angle[abajo]-angle[n]-fy);
        float O_x = mod_2pi2(angle[derecha]-angle[n]-fx);
        float O_z = mod_2pi2(angle[atras]-angle[n]);

        Energy[n] = Vx(O_y)+Vx(O_x);//-cosf(O_z)/gamma;
        dO_z[n] = O_z;
    }
}
}

extern "C" {
__device__ float deriv_Vxy(float x)
{
    //REAL r=0.37565;
    //return -0.75*r-(1.0-r)*cos(x)-(r*0.25)*cos(2.0*x);
    return 0.62435*sinf(x)+0.187825*sinf(2.0*x);
    //return -cos(x);
}
}

extern "C" {
__global__ void current(float *angle, float *jx, float *jy, float *Ax, float *Ay, int Lxy, int Lz, float frustration)
{   /*
    Calcula la energia por sitio a partir del array de phases, como para luego realizar una reduccion
    y obtener la energia total.
    */
    int N=Lxy*Lxy*Lz;
    int n=blockIdx.x*blockDim.x+threadIdx.x;
    if (n<N){

        int nx=n%Lxy;
        int ny=int(n/Lxy)%Lxy;
        int nz=int(n/Lxy/Lxy);
        int n2d = nx + ny*Lxy;


        int abajo = nx+((ny+1)%Lxy)*Lxy+nz*Lxy*Lxy;
        int derecha = (nx+1)%Lxy+ny*Lxy+nz*Lxy*Lxy;

        float fy = 2*PI*frustration*nx+Ay[n2d];
        float fx = Ax[n2d];

        float O_y = mod_2pi2(angle[abajo]-angle[n]-fy);
        float O_x = mod_2pi2(angle[derecha]-angle[n]-fx);

        jx[n] = deriv_Vxy(O_x);
        jy[n] = deriv_Vxy(O_y);
    }
}
}
