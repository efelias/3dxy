import numpy as np

__all__ = ['strucuture_factor']

def delete(ft, x, y, Lxy, dx=2):
    for i in range(-dx, dx):
        for j in range(-dx, dx):
            ft[(y+i+Lxy)%Lxy, (x+j+Lxy)%Lxy] = 0.0

def find_local(ft, Lxy, n = 6):
    ft[0,0] = 0.
    maxi = ft
    maximos = []
    for i in range(n):
        m = np.argmax(maxi)
        y, x = int(m//Lxy), int(m%Lxy)
        maximos.append([ft[y,x],x,y])
        delete(maxi, x, y, Lxy)
        #delete(maxi,Lxy-x,Lxy-y)
    return maximos 

def strucuture_factor(carga, Lxy, Lz, frus, n=12):

    ft = np.zeros((Lxy,Lxy), dtype=np.float64)

    for i in range(Lz):
        ft += np.abs(np.fft.fft2(carga[i]))**2

    ft = ft/frus/Lxy**2/Lz
    ftc = ft.copy()
    lm = np.array(find_local(ft, Lxy, n=n))
#    print('maximos##############################')
#    print(lm)

    return np.fft.fftshift(ftc), np.array(lm[:,1], dtype=np.int32), np.array(lm[:,2], dtype=np.int32)

