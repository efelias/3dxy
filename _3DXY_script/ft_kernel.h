
#define PI 3.141592654f

extern "C" {
__global__ void J_ft(float *j_alpha, float *aq_alpha_re, float *aq_alpha_im, int qx, int qy,
	                 float SM, int Lxy, int Lz, int axis)
{
	int N = Lxy*Lxy*Lz;
	int n = blockIdx.x*blockDim.x+threadIdx.x;

	if (n<N){
		int nx = n%Lxy;
        int ny = int(n/Lxy)%Lxy;
        //int nz = int(n/Lxy/Lxy);
        float factor = 2.0*(2.0-cosf(2*PI*qx/Lxy)-cosf(2*PI*qy/Lxy))/SM;
        //float factor = ((2*PI*qx/Lxy)*(2*PI*qx/Lxy)+(2*PI*qy/Lxy)*(2*PI*qy/Lxy))/SM;

        float fase = 2*PI*((nx+.5*(1-axis))*qx+(ny+.5*axis)*qy)/Lxy;
        //float fase = 2*PI*((nx+.5*(axis))*qx+(ny+.5*axis)*qy)/Lxy;
        float coseno = cosf(fase);
        float seno = sinf(fase);
        aq_alpha_re[n] = j_alpha[n]*coseno/factor;
        aq_alpha_im[n] = j_alpha[n]*seno/factor;
	}
}
}

extern "C" {
__global__ void Inv_ft_a(float *a_alpha, float *aq_alpha_re, float *aq_alpha_im, int *qx, int *qy,
	                     int nq, int Lxy, int axis)
{	/*
	a_alpha.shape =  (Lxy**2,)
	the rest have shape (nq,)
	*/
	int N = Lxy*Lxy;
	int n = blockIdx.x*blockDim.x+threadIdx.x;

	if (n<N){
		int nx = n%Lxy;
        int ny = int(n/Lxy)%Lxy;

        a_alpha[n] = 0.0;
        
        for (int i=0;i<nq;i++){
	        float fase = 2*PI*((nx+.5*(1-axis))*qx[i]+(ny+.5*axis)*qy[i])/Lxy;
        	//float fase = 2*PI*((nx+.5*(axis))*qx[i]+(ny+.5*axis)*qy[i])/Lxy;
	        float coseno = cosf(fase);
	        float seno = sinf(fase);
	        a_alpha[n] += coseno*aq_alpha_re[i]+seno*aq_alpha_im[i];
	    }
	}
}
}
