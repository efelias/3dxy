#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function
from time import time
import sys
#############################################
from .gpu_functions import *
import pycuda.autoinit
import pycuda.gpuarray as gy
import pycuda.cumath as cm
#############################################
import numpy as np
import os
from collections import OrderedDict
from .h5_functions import *
from .extra_functions import *
from .vortex_class import *

__all__ = ['run_XY', 'default_parm_dict', 'random_phase', 'half_ordered',
           'empty_state_dict', 'main']

def run_XY(intial_cond, pasos_totales, parms_dict, **state_dict):

    start = time()

    system = vortex(intial_cond, parms_dict, **state_dict)

    print('Runing dynamics...\n')
    print('parameters:\n')
    for k, v in parms_dict.items():
        print("{:12s}:\t{}".format(k, v))
    print("{:12s}:\t{}\n".format('MC steps', pasos_totales))
 	###############
    #Termalizacion
    ###############
    if parms_dict['term_steps'] > 0:
        print('Termalizing...\n')
        t_speps = 0

        while t_speps < parms_dict['term_steps']//50:
        	t_speps2 = 0
        	while t_speps2 < 50:
        		system.one_step()
        		t_speps2 += 1
        	t_speps += 1
        	system.magnetic_coupling()
        	print("{:.3f} percent complete".format(float(t_speps)*50*100/parms_dict['term_steps']), end='\r')
        	sys.stdout.flush()
    print("\n")
    ###############
    #Midiendo
    ###############
    print('Messuring...\n')
    counter1 = 0
    while counter1 < pasos_totales/50:
        counter2 = 0
        while counter2 < 50:
            system.one_step()
            counter2 += 1
            system.messure_globals()
            system.acumulate_carge(compute=True)
        system.magnetic_coupling()
        counter1 += 1
        print("{:.3f} percent complete".format(float(counter1)*50*100/pasos_totales), end='\r')
        sys.stdout.flush()
    print('\n')
    system.messure_locals(pasos_totales)
    stop = time()
    
    return system.current_state_dict, stop - start

def default_parm_dict(T=.2):
    """Generate a dictionary with the default parameter"""
    parms_dict = OrderedDict()
    parms_dict['term_steps'] = np.int32(1000)
    parms_dict['SM'] = np.float32(.0001)
    parms_dict['temperatura'] = np.float32(T)
    parms_dict['Lxy'] = np.int32(72)
    parms_dict['Lz'] = np.int32(10)
    parms_dict['frustration'] = np.float32(2.0/72)
    parms_dict['gamma'] = np.float32(225)
    parms_dict['seed'] = np.int32(1)

    return parms_dict

def random_phase(N):
    """Generate a random phase configuration with lenght N = Lxy*Lxy*Lz"""
    ran = np.random.rand(N)
    In2 = np.pi*(2*np.array(ran, dtype=np.float32) - np.ones((N,), dtype=np.float32))
    return In2

def half_ordered(path_to_ordered_config, Lxy, Lz):
    """Generate a ordered-random configuration of intercalated layers"""
    ordered = np.genfromtxt(path_to_ordered_config, dtype=np.float32)
    temp_conf = np.append(ordered, random_phase(Lxy**2))
    return np.tile(temp_conf, Lz//2)

def empty_state_dict():
    return {'cosz' : [], 'sinz' : [], 'carga' : [], 'energia' : [],
            'aceptancia' : [], 'paso' : 0, 'phases' : [], 'campo': [], 'mean_carge':[]}

def main():
    initial_state_dict = empty_state_dict()

    parms_dict = default_parm_dict()

    N = parms_dict['Lxy']**2*parms_dict['Lz']

    current_state_dict, delta_t = run_XY(random_phase(N), 10000, parms_dict, **initial_state_dict)
    
    h5f = h5file('test')
    h5f.write('random', 10000, delta_t, parms_dict, **current_state_dict)

