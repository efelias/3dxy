#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function

from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg
import pyqtgraph.opengl as gl
import numpy as np

import argparse
import numpy as np
import sys
from collections import OrderedDict

parse = argparse.ArgumentParser('3D XY Montecarlo for Layered Type II Superconductors')

parse.add_argument('-Tm_steps', type=np.float32, help='termalization steps', default=np.int32(100000))

parse.add_argument('-SM', type=np.float32, help='Magnetic penetration', default=np.float32(.0001))

parse.add_argument('-T', type=np.float32, help='Temperatura', default=np.float32(.02))

parse.add_argument('-Lxy', type=np.int32, help='Tamaño del lado en el plano XY', default=np.int32(72))

parse.add_argument('-Lz', type=np.int32, help='Número de capas en el eje Z', default=np.int32(4))

parse.add_argument('-f', type=np.float32, help='Valor  de la frustración según f = m/Lxy, donde m enteros', default=np.float32(1./36.))

parse.add_argument('-G', type=np.float32, help='Anisotropía en el eje Z', default=np.float32(25000))

parse.add_argument('-ins', type=np.int32, help='Paso de inicio, importante para la generación de los números aleatoreos', default=np.int32(0))

parse.add_argument('-ms', type=np.int32, help='Pasos montecarlo', default=np.int32(50000))

parse.add_argument('-seed', type=np.int32, help='semilla', default=np.int32(0))

args = parse.parse_args()

parms_dict = OrderedDict()    

parms_dict['term_steps'] = args.Tm_steps
parms_dict['SM'] = args.SM
parms_dict['temperatura'] = args.T
parms_dict['Lxy'] = args.Lxy
parms_dict['Lz'] = args.Lz
parms_dict['frustration'] = np.float32(args.f)
parms_dict['gamma'] = args.G
parms_dict['seed'] = args.seed

m_steps = args.ms

import _3DXY_script as xy

app = QtGui.QApplication([])
w = gl.GLViewWidget()

w2 = QtGui.QMainWindow()
w2.resize(750,1000)
cw = QtGui.QWidget()
cw2 = QtGui.QWidget()
layout = QtGui.QGridLayout()
layout2 = QtGui.QGridLayout()

cw.setLayout(layout)
cw2.setLayout(layout2)
layout2.addWidget(cw)
w2.setCentralWidget(cw2)

changingLabel = QtGui.QLabel()  ## updated immediately
changedLabel = QtGui.QLabel()   ## updated only when editing is finished or mouse wheel has stopped for 0.3sec
changingLabel.setMinimumWidth(200)
labels = []


spins = [
    ("Temperatura, min=0, no maximum.", 
     pg.SpinBox(value=parms_dict['temperatura'], bounds=[0, None])),
]

def valueChanged(sb):
    global system
    changedLabel.setText("Final value: %s" % str(sb.value()))
    system.change_parm('temperatura',sb.value())
    print(system.parms)

def valueChanging(sb, value):
    changingLabel.setText("Value changing: %s" % str(sb.value()))

    
for text, spin in spins:
    label = QtGui.QLabel(text)
    labels.append(label)
    layout.addWidget(label)
    layout.addWidget(spin)
    spin.sigValueChanged.connect(valueChanged)
    spin.sigValueChanging.connect(valueChanging)

layout.addWidget(changingLabel, 0, 1)
layout.addWidget(changedLabel, 2, 1)

Lxy = parms_dict['Lxy']
Lz = parms_dict['Lz']

#system = xy.vortex(xy.random_phase(Lxy**2*Lz), parms_dict)
system = xy.vortex(xy.half_ordered('ONE_LAYER_Lxy_72_FRUS_2_ordered.conf', parms_dict['Lxy'], parms_dict['Lz']), parms_dict)

#system.make_order(True)

w.opts['distance'] = 2*Lxy
w.show()
w2.show()

#w.setWindowTitle('pyqtgraph example: GLScatterPlotItem')

g = gl.GLGridItem()
g.setSize(Lxy,Lxy)
w.addItem(g)

#win = QtGui.QMainWindow()
#win.resize(600,600)
#cw2 = QtGui.QWidget()
imv = pg.ImageView()
#layout2 = QtGui.QGridLayout()
#cw2.setLayout(layout2)
#win.setCentralWidget(cw2)
#win.show()

#win2 = QtGui.QMainWindow()
#win2.resize(600,600)
imv2 = pg.ImageView()
layout2.addWidget(imv)
layout2.addWidget(imv2)
#win.show()
#win2.setCentralWidget(imv2)
#win2.show()
## Create random 3D data set with noisy signals
def ft(c):
    val = np.fft.fftshift(np.abs(np.fft.fft2(c[0]))**2)
    for i in range(1, c.shape[0]):
        val+=np.fft.fftshift(np.abs(np.fft.fft2(c[i]))**2)
    val/=c.shape[0]
    return np.log(val)

system.one_step()
system.magnetic_coupling()

c = system.get_charge(compute=True).get().reshape((Lz, Lxy, Lxy))

def get_shift(c, value, Lxy):
	pos = np.roll(np.argwhere(c==value), 2, axis=1)
	pos[:,1]-=Lxy//2
	pos[:,0]-=Lxy//2
	return pos

pos = get_shift(c, 1, Lxy)
pos2 = get_shift(c, -1, Lxy)

sp2 = gl.GLScatterPlotItem(pos=pos, color=(0,1,.5,1), size=5)
sp3 = gl.GLScatterPlotItem(pos=pos2, color=(1, 0,.5,1), size=5)

w.addItem(sp2)
w.addItem(sp3)

imv.setImage(np.log(system.get_ft()))#, xvals=np.linspace(1., 3., data.shape[0]))
imv2.setImage(system.get_field().get().reshape((Lxy, Lxy)))
level = parms_dict['SM']*1e-5
imv2.setLevels(-level, level)
## Set a custom color map

colors = [
    (0, 0, 0),
    (45, 5, 61),
    (84, 42, 55),
    (150, 87, 60),
    (208, 171, 141),
    (255, 255, 255)
]

# colors = [
#     (148, 0, 211),
#     (75, 0, 130),  
#     (0, 0, 255),
#     (0, 255, 0),
#     (255, 255, 0), 
#     (255, 127, 0),     
#     (255, 0 , 0)
# ]

cmap = pg.ColorMap(pos=np.linspace(0.0, 1.0, 6), color=colors)
imv.setColorMap(cmap)
imv2.setColorMap(cmap)


count = 1
def update():
    # update volume colors
    global sp2, sp3, imv, system, Lxy, Lz, imv2

    cont = 0
    while cont < 5:
        system.one_step()
        cont+=1
    system.magnetic_coupling()

    c = system.get_charge(compute=True).get().reshape((Lz, Lxy, Lxy))
    pos = get_shift(c, 1, Lxy)
    pos2 = get_shift(c, -1, Lxy)
    imv.setImage(np.log(system.get_ft()))
    imv2.setImage(system.get_field().get().reshape((Lxy, Lxy)))
    imv2.setLevels(-level, level)
    sp2.setData(pos=pos)
    sp3.setData(pos=pos2)
   
t = QtCore.QTimer()
t.timeout.connect(update)
t.start(1)

if __name__ == '__main__':
    import sys
    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()

