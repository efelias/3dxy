#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import h5py as h5
import sys
from collections import OrderedDict
import numpy as np

parse = argparse.ArgumentParser('3D XY Montecarlo for Layered Type II Superconductors, loding from HDF file')

parse.add_argument('-h5_file', type=str, help='HDF5 File path')

args = parse.parse_args()

def make_t_list(gamma, frus, n_points=32):
     tm = .33/np.sqrt(frus*gamma)
     
     delta = 0.015
     n_delta = 8
 
     tmin = tm-n_delta*delta
 
     while tmin < 0:
         tmin += delta/4
         
     tmax = tm + (n_delta-4)*delta
 
     print(tm, tmin, tmax)
 
     tt = np.linspace(tmin, tmax, n_points,)[::-1]
 
     temps = np.concatenate((tt, tt[:-1][::-1]))
 
     return temps

if __name__ == '__main__':
    file_path = args.h5_file
    
    f = h5.File(file_path, 'r')
    
    keys = list(f.keys())
    
    g = f[keys[-1]] 
    vals = len(keys)
    assert vals < 63

    from _3DXY_script import *
    
    parms_dict = default_parm_dict()

    print('reading parameters...\n')
    for k, v in g.attrs.items():
        if k in parms_dict.keys():
            print(k, v)
            parms_dict[k] = v

    paso = g['paso'].value
    print('last step = {}'.format(paso))
    
    init_conf = g['phases'].value
    
    MC_steps = g.attrs['MC_steps']
    f.close()
    
    temps = make_t_list(parms_dict['gamma'], parms_dict['frustration'])[vals:]

    parms_dict['temperatura'] = temps[0]

    h5f = h5file(file_path[:-3])

    current_state_dict, delta_t = run_XY(init_conf.flatten(), MC_steps, parms_dict, paso=paso)

    h5f.write('helf-half', m_steps, delta_t, parms_dict, new_group=True, **current_state_dict) 
    
    for T in temps[1:]:
        parms_dict['temperatura'] = T

        current_state_dict, delta_t = run_XY(current_state_dict['phases'].flatten(), m_steps, parms_dict,
                                             paso=current_state_dict['paso'])

        h5f.write('helf-half', m_steps, delta_t, parms_dict, new_group=True, **current_state_dict) 
    
